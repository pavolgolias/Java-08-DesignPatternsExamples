﻿## Example implementations of design patterns

This project contains example implementations of chosen design patterns.K 

 - Factory
 - Abstract factory 
 - Builder
 - Prototype
 - Adapter
 - Bridge
 - Composite
 - Decorator
 - Facade
 - Proxy
 - Chain of responsibility 
 - Command
 - Mediator
 - Observer
 - State
 - Template 
