package sk.pgl.designpatterns.prototype;

// https://www.tutorialspoint.com/design_pattern/prototype_pattern.htm
// Prototype pattern refers to creating duplicate object while keeping performance in mind. This type of design pattern
// comes under creational pattern as this pattern provides one of the best ways to create an object.
public class PrototypePatternDemo {

    public static void main(String[] args) {
        ShapeCache.loadCache();

        Shape clonedShape = ShapeCache.getShape("1");
        System.out.println("Shape : " + clonedShape.getType());
        clonedShape.draw();

        Shape clonedShape2 = ShapeCache.getShape("2");
        System.out.println("Shape : " + clonedShape2.getType());
        clonedShape2.draw();

        Shape clonedShape3 = ShapeCache.getShape("3");
        System.out.println("Shape : " + clonedShape3.getType());
        clonedShape3.draw();
    }
}
