package sk.pgl.designpatterns.command;

public interface Order {
    void execute();
}
