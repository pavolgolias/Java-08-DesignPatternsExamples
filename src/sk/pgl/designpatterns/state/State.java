package sk.pgl.designpatterns.state;

public interface State {
    void doAction(Context context);
}
