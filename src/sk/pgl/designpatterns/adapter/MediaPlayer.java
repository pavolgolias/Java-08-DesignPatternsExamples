package sk.pgl.designpatterns.adapter;

public interface MediaPlayer {
    void play(String audioType, String fileName);
}
