package sk.pgl.designpatterns.facade;

// https://www.tutorialspoint.com/design_pattern/facade_pattern.htm

// Facade pattern hides the complexities of the system and provides an interface to the client using which the client
// can access the system. This type of design pattern comes under structural pattern as this pattern adds an interface
// to existing system to hide its complexities.

public class FacadePatternDemo {
    public static void main(String[] args) {
        ShapeMaker shapeMaker = new ShapeMaker();

        shapeMaker.drawCircle();
        shapeMaker.drawRectangle();
        shapeMaker.drawSquare();
    }
}
