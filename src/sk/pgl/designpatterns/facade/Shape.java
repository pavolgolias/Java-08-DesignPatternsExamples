package sk.pgl.designpatterns.facade;

public interface Shape {
    void draw();
}
