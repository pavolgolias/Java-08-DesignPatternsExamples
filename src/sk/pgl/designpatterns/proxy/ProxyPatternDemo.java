package sk.pgl.designpatterns.proxy;

// https://www.tutorialspoint.com/design_pattern/proxy_pattern.htm

// In proxy pattern, a class represents functionality of another class. This type of design pattern comes under structural pattern.
// In proxy pattern, we create object having original object to interface its functionality to outer world.

public class ProxyPatternDemo {
    public static void main(String[] args) {
        Image image = new ProxyImage("test_10mb.jpg");
        image.display();
        image.display();
        image.display();
        image.display();
    }
}
