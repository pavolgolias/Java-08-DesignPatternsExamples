package sk.pgl.designpatterns.proxy;

public interface Image {
    void display();
}
