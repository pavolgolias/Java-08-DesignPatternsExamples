package sk.pgl.designpatterns.factory;

class ShapeFactory {
    static final String SHAPE_RECTANGLE = "rectangle";
    static final String SHAPE_SQUARE = "square";

    Shape getShape(String shapeType) {
        if (shapeType == null) {
            return null;
        }

        if (SHAPE_SQUARE.equals(shapeType)) {
            return new Square();
        } else if (SHAPE_RECTANGLE.equals(shapeType)) {
            return new Rectangle();
        }

        return null;
    }
}
