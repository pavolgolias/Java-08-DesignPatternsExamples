package sk.pgl.designpatterns.factory;

// https://www.tutorialspoint.com/design_pattern/factory_pattern.htm
public class FactoryDemo {
    public static void main(String[] args) {
        ShapeFactory shapeFactory = new ShapeFactory();

        Shape rectangleShape = shapeFactory.getShape(ShapeFactory.SHAPE_RECTANGLE);
        rectangleShape.draw();

        Shape squareShape = shapeFactory.getShape(ShapeFactory.SHAPE_SQUARE);
        squareShape.draw();
    }
}
