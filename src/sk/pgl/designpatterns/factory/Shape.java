package sk.pgl.designpatterns.factory;

public interface Shape {
    void draw();
}
