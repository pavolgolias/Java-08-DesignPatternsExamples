package sk.pgl.designpatterns.factory.abstractfactory;

import sk.pgl.designpatterns.factory.Shape;

// https://www.tutorialspoint.com/design_pattern/abstract_factory_pattern.htm
public class AbstractFactoryDemo {
    public static void main(String[] args) {
        AbstractFactory shapeFactory = FactoryProducer.getFactory(FactoryProducer.FACTORY_SHAPE);

        Shape shape1 = shapeFactory.getShape(ShapeFactory.SHAPE_RECTANGLE);
        shape1.draw();

        Shape shape2 = shapeFactory.getShape(ShapeFactory.SHAPE_SQUARE);
        shape2.draw();

        AbstractFactory colorFactory = FactoryProducer.getFactory(FactoryProducer.FACTORY_COLOR);

        Color color1 = colorFactory.getColor(ColorFactory.COLOR_GREEN);
        color1.fill();

        Color color2 = colorFactory.getColor(ColorFactory.COLOR_RED);
        color2.fill();
    }
}
