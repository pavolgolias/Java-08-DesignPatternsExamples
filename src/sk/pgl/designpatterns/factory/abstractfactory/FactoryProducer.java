package sk.pgl.designpatterns.factory.abstractfactory;

class FactoryProducer {
    static final String FACTORY_SHAPE = "SHAPE";
    static final String FACTORY_COLOR = "COLOR";

    static AbstractFactory getFactory(String choice) {

        if (FACTORY_SHAPE.equals(choice)) {
            return new ShapeFactory();
        } else if (FACTORY_COLOR.equals(choice)) {
            return new ColorFactory();
        }

        return null;
    }
}
