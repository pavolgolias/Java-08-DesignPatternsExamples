package sk.pgl.designpatterns.factory.abstractfactory;

import sk.pgl.designpatterns.factory.Shape;

public class ColorFactory extends AbstractFactory {
    static final String COLOR_RED = "red";
    static final String COLOR_GREEN = "green";

    @Override
    Color getColor(String color) {
        if (color == null) {
            return null;
        }

        if (COLOR_RED.equals(color)) {
            return new RedColor();
        } else if (COLOR_GREEN.equals(color)) {
            return new GreenColor();
        }

        return null;
    }

    @Override
    Shape getShape(String shape) {
        return null;
    }
}
