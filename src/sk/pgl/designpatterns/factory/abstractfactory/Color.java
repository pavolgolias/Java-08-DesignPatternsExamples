package sk.pgl.designpatterns.factory.abstractfactory;

public interface Color {
    void fill();
}
