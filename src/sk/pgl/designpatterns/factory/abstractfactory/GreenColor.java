package sk.pgl.designpatterns.factory.abstractfactory;

public class GreenColor implements Color {
    @Override
    public void fill() {
        System.out.println("Inside Green::fill() method.");
    }
}
