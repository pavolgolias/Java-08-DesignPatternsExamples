package sk.pgl.designpatterns.factory.abstractfactory;

import sk.pgl.designpatterns.factory.Rectangle;
import sk.pgl.designpatterns.factory.Shape;
import sk.pgl.designpatterns.factory.Square;

public class ShapeFactory extends AbstractFactory {
    static final String SHAPE_RECTANGLE = "rectangle";
    static final String SHAPE_SQUARE = "square";

    @Override
    Color getColor(String color) {
        return null;
    }

    @Override
    Shape getShape(String shape) {
        if (shape == null) {
            return null;
        }

        if (SHAPE_SQUARE.equals(shape)) {
            return new Square();
        } else if (SHAPE_RECTANGLE.equals(shape)) {
            return new Rectangle();
        }

        return null;
    }
}
