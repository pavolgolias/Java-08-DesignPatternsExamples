package sk.pgl.designpatterns.factory.abstractfactory;

import sk.pgl.designpatterns.factory.Shape;

public abstract class AbstractFactory {

    abstract Color getColor(String color);

    abstract Shape getShape(String shape);
}
