package sk.pgl.designpatterns.builder;

public interface Item {
    String name();

    Packing packing();

    float price();
}
