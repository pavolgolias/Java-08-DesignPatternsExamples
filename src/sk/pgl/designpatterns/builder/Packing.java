package sk.pgl.designpatterns.builder;

public interface Packing {
    String pack();
}
