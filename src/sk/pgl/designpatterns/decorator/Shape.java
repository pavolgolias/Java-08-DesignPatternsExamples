package sk.pgl.designpatterns.decorator;

public interface Shape {
    void draw();
}
